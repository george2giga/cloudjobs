# Docker

### Overview
Gitlab introduced a pipeline template for .NET Core projects:
https://gitlab.com/gitlab-org/project-templates/dotnetcore. 

The RetroMood solution expands on the basic **.gitlab-ci.yml** by adding:
* Multi stage pipeline (buid - test - release) using conditional branches. 
Building and testing on **DEV** branch only, creating a release image only from **MASTER**.

* Building and pushing a docker image for the website in the project registry.

The pipeline uses DIND https://hub.docker.com/r/gitlab/dind/ in order to be able to build an image using the Gitlab shared runners.