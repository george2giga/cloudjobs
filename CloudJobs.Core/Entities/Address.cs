﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudJobs.Core.Entities
{
    public class Address : BaseEntityAudited
    {
        public string City { get; set; }
        public string Street { get; set; }
    }
}
