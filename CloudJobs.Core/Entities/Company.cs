﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudJobs.Core.Entities
{
    public class Company : BaseEntityAudited
    {
        public string Name { get; set; }
        public string CompanyUrl { get; set; }
        public Address Address { get; set; }
    }
}
