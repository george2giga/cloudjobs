﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudJobs.Core.Entities
{
    public class JobPost : BaseEntityAudited
    {
        public Company Company { get; set; }
        public Address Address { get; set; }

        public string Title { get; set; }
        public string Content { get; set; }
    }
}
