﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudJobs.Core.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
