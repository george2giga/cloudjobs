﻿using System;
using Xunit;

namespace CloudJobs.Core.Tests
{
    public class Class1
    {
        [Fact]
        public void TestSample()
        {
            var result = 1 + 1;

            Assert.Equal(2, result);
        }
    }
}
