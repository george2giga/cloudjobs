﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudJobs.Core.Entities
{
    public class BaseEntityAudited : BaseEntity
    {
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
