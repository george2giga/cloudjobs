﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CloudJobs.Core.Entities
{
    public class JobApplication : BaseEntityAudited
    {
        public JobPost JobPost { get; set; }
        public string IntroductionLetter { get; set; }
        public Address Address { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string WebSite { get; set; }
        public string CvAttachment { get; set; }
    }
}
